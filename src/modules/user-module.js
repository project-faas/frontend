import axios from 'axios'

const state = {
  user: {},
  message: ""
}

const getters = {
  userList: state => state.user
}

const actions = {
  async register({commit}, user) {
    const response = await axios.post('http://main-api:8080/api/users/register', user)
    commit("register", response.data)
  },
  async login({commit}, user) {
    const response = await axios.get('http://main-api:8080/api/users/login', user)
    commit("login", response.data.user, response.data.message)
  },
  async logout({commit}) {
    commit("logout")
  }
}

const mutations = {
  register: (state, message) => {
    state.message = message
  },
  login: (state, user, message) => {
    state.user = user
    state.message = message
  },
  logout: (state) => {
    state.user = {}
    state.message = "Logged out!"
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}