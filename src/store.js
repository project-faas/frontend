import { createStore } from 'vuex'
import UserModules from './modules/user-module.js'

export default new createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    UserModules
  }
})