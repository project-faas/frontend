import NotFound from './views/NotFound.vue'

/** @type {import('vue-router').RouterOptions['routes']} */
export const routes = [
  {
    path: '/',
    component: () => import('./views/Home.vue'),
    meta: { title: 'Home' }
  },
  {
    path: '/about',
    meta: { title: 'About' },
    component: () => import('./views/About.vue')
  },
  {
    path: '/login',
    meta: {title: 'login'},
    component: () => import('./views/Login.vue')
  },
  {
    path: '/register',
    meta: {title: 'register'},
    component: () => import('./views/Register.vue')
  },
  { path: '/:path(.*)', component: NotFound },
]
